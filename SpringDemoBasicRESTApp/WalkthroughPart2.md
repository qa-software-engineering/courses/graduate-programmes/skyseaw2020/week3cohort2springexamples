# Creating a Simple Spring Boot REST Application - Part 2 - Simple Tests

## Using JUnit to Test Application Configuration

1. Ensure that `spring-boot-starter-test` dependency exists in `pom.xml`;
2. Open `DemoApplicationTests` from `/test/java/com.example.demo`;
3. Explain that `@SpringBootTest` annotation makes test runner search for a class that is annotated with the `@SpringBootConfiguration` and this is the `DemoApplication` class as `@SpringBootApplication` sets the **config**, creating an application context very similar to the actual application;
4. The `contextLoads` test does exactly what it says on the tin - makes sure that the application can start!
5. Running the test starts up an instance of the application and returns a passing state.

## Smoke Tests

Useful for ensuring that the context is actually creating items, such as controllers.

1. Create a class called `SmokeTest` in the `test/java/com.example.demo` folder;
2. Declare a `private DemoController controller` and prefix with the `@Autowired` notation

The `@Autowired` annotation is how Spring helps you to automatically configure injections into your code.  Without these annotations you would have to tell Spring where to find the symbol - usually in some form of configuration file.

3. Create `@Test` called `contextLoads` and use `assertThat` from `assertj.core.api.Assertions` to check that the `DemoController` is actually instantiated.

Note that all controllers and any other *smoke tests* could be performed in this file - indeed they could have been done in the initial DemoApplicationTests but this couples that standard test to the items in this application.

4. Running the test starts up an application and should return a passing test.

## HTTP Request Test

The next step is to test that an HTTP request to the application returns the correct data.

1. Create a class called `HttpRequestTest`;
2. Annotate the class with `@SpringBootTest` supplying addition meta-data of `webEnvironment` set to `SpringBootTest.WebEnvironment.RANDOM_PORT`
   - This essentially creates a mock environment for running the application and sets the server port to a random value
3. Define `port` by setting it as a `private int` and annotating it with `@LocalServerPort` - this injects the value to be used for `port` at runtime.
4. `@Autowired` a `private TestRestTemplate` called `restTemplate` - this is a special class for testing that is a template for HTTP requests - its automatically available as we have used the `@SpringBootTest` annotation on the class.
5. The `@Test` checks to see if the `hello()` method in the `DemoController` returns the correct values when a request is made to the default route.

### Activity 1

Create a test for the `/?name=Something` something route in the `DemoController`

## Using POSTMAN to test Request/Responses

Postman requires the application running locally, so fire up `DemoApplication`

1. Demonstrate a **GET request** test and how these can be saved as a Collection for use in Jenkins.

### Activity 2

Create a Postman test for the `/?name=Something` something route in the `DemoController`

**Take a break!**
