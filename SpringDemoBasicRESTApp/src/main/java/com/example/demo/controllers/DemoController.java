package com.example.demo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DemoController {

    /**
     * Makes GET requests to the "/" route return a String
     * The @GetMapping annotation tells Spring to use the method defined beneath it for GET requests to /hello
     * The @ResponseBody annotation on the return type binds the return value to the web response body.
     * The @RequestParam tells Spring how to deal with a ?name=<someValue> extension to the "/" route and
     * tells the method to use a defaultValue of "World" if none is supplied for name.
     * This annotates the String argument name in the hello method.
     *
     * @param   name    a String that is either the value from the GET request or "World"
     * @return          a String that contains the name parameter
     */
    @GetMapping("/")
    public @ResponseBody String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }


}
