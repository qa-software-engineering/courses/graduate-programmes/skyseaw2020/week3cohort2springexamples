package com.example.demo.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TodoWriterService {

    public ResponseEntity addTodo(Object todo) {
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    public void updateTodo(Object todo) {

    }
}
