package com.example.demo.controller;

import com.example.demo.services.TodoWriterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class TodoWriteController {

    @Autowired
    private TodoWriterService todoWriterService;

    @PostMapping("/todos")
    public ResponseEntity addTodo(Object todo) {

        return todoWriterService.addTodo(todo);
    }

    // Add route mapping to update based on ID
}
