package com.example.demo.controller;

import com.example.demo.services.TodoReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TodoReadController {

    @Autowired
    private TodoReaderService todoReaderService;

    @GetMapping("/todos")
    public Object getAllTodos() {
        return todoReaderService.getAll();
    }

    // Add mapping to get single based on its ID

}
