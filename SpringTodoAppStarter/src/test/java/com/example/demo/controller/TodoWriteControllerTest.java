package com.example.demo.controller;

import com.example.demo.services.TodoWriterService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TodoWriteController.class)
public class TodoWriteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoWriterService todoWriterService;

    @Test
    public void postTodosWithObjectShouldReturn200 () throws Exception {
        // Arrange
        Object mockTodo = new Object();
        when(todoWriterService.addTodo(mockTodo)).thenReturn(ResponseEntity.ok(HttpStatus.CREATED));

        // Act and Assert
        this.mockMvc.perform(post("/todos"))
                .andExpect(status().isOk());
    }

    // Add test for update based on Id
}
