package com.example.demo.controller;

import com.example.demo.services.TodoReaderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TodoReadController.class)
public class TodoReadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoReaderService todoReaderService;

    @Test
    public void getTodosShouldReturnList () throws Exception {
        // Arrange
        List<Object> mockTodos = new ArrayList<>();
        when(todoReaderService.getAll()).thenReturn(mockTodos);

        // Act and Assert
        this.mockMvc.perform(get("/todos"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(mockTodos.toString()));
    }

    // Add test for get single by Id

}
