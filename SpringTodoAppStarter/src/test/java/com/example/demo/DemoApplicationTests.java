package com.example.demo;

import com.example.demo.controller.TodoReadController;
import com.example.demo.controller.TodoWriteController;
import com.example.demo.services.TodoReaderService;
import com.example.demo.services.TodoWriterService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private TodoReadController todoReadController;

	@Autowired
	private TodoReaderService todoReaderService;

	@Autowired
	private TodoWriteController todoWriteController;

	@Autowired
	private TodoWriterService todoWriterService;

	@Test
	void contextLoads() {

		assertThat(todoReadController).isNotNull();
		assertThat(todoReaderService).isNotNull();
		assertThat(todoWriteController).isNotNull();
		assertThat(todoWriterService).isNotNull();
	}

}
